import { app, protocol } from "electron"
import windowHelper from "./windowHelper"
import store from "@/store"
import ipc from "electron-ipc-extra"
import is from "./is"
const path = require("path")
import { createProtocol } from "vue-cli-plugin-electron-builder/lib"
protocol.registerSchemesAsPrivileged([
  { scheme: "app", privileges: { secure: true, standard: true } }
])
//目录信息
let appFolder = `${path.dirname(process.execPath)}\\`
if (!app.isPackaged) {
  //开发环境下的资源目录
  appFolder = `${process.cwd()}\\static\\`
}
let resourcesPath = `${appFolder}resources\\`
let _controller
//创建主窗口
function createMainWindow() {
  windowHelper.open("index", "index", {
    width: 800,
    height: 600
  })
}
const APP = {
  //状态数据
  state: {
    //程序目录
    appFolder,
    //程序资源目录
    resourcesPath,
    //用户数据文件夹
    userData: `${app.getPath("userData")}\\`
  },
  //启动
  Start(controller) {
    _controller = controller
    //单例模式,防止打开两个程序
    if (!app.requestSingleInstanceLock()) {
      return app.quit()
    } else {
      app.on("second-instance", () => {
        // 当运行第二个实例时,将会聚焦到myWindow这个窗口
        let win = windowHelper.get("index")
        if (win) {
          if (win.isMinimized()) win.restore()
          win.focus()
          win.show()
        }
      })
    }
    //监听vuex
    controller.forEach(c => {
      c.beforeCreate && c.beforeCreate.call(APP)
    })
    controller.forEach(c => {
      c.created && c.created.call(APP)
    })
    //创建主窗口
    app.on("ready", () => {
      createProtocol("app")
      createMainWindow() //创建主窗口
      controller.forEach(c => {
        c.beforeMount && c.beforeMount.call(APP)
      })
      // console.log("000", new Date().getTime())
      ipc.on("$$AppReady", () => {
        // console.log("AppReady", new Date().getTime())
        store.commit = (type, payload) =>
          ipc.send("vuex$commit", { type, payload })
        store.dispatch = (type, payload) =>
          ipc.send("vuex$dispatch", { type, payload })
        controller.forEach(c => {
          c.methods &&
            Object.keys(c.methods).forEach(cmd => {
              ipc.off(cmd)
              ipc.on(cmd, c.methods[cmd])
            })
          c.mounted && c.mounted.call(APP)
        })
      })
    })
    // Quit when all windows are closed.
    app.on("window-all-closed", () => {
      // On macOS it is common for applications and their menu bar
      // to stay active until the user quits explicitly with Cmd + Q
      if (process.platform !== "darwin") {
        let isbreak = false
        controller.forEach(c => {
          if (c.beforeDestroy && c.beforeDestroy.call(APP) === false) {
            isbreak = true
            return false
          }
        })
        if (isbreak) return false
        controller.forEach(c => {
          c.destroyed && c.destroyed.call(APP)
        })
        setTimeout(app.quit, 0)
      }
    })
    app.on("activate", () => {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (win === null) {
        createMainWindow()
      }
    })
  },
  //触发自定义事件
  emit(type, ...args) {
    _controller.forEach(c => {
      c.methods &&
        Object.keys(c.methods).forEach(cmd => {
          if (cmd == type) {
            c.methods[cmd].apply(c, args)
          }
        })
    })
  }
}
// 在开发模式下，应父进程的要求完全退出。
if (is.dev) {
  if (is.windows) {
    process.on("message", data => {
      if (data === "graceful-exit") {
        app.quit()
      }
    })
  } else {
    process.on("SIGTERM", () => {
      app.quit()
    })
  }
}

//启动程序
export default APP
