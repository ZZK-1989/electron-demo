import fs from "fs"
//文件操作
export default {
  //读取文本文件
  readAllTextSync(path) {
    try {
      if (fs.existsSync(path)) {
        return fs.readFileSync(path, "utf8")
      }
    } catch (err) {
      console.error(err)
    }
  },
  //写入文本文件
  writeAllTextSync(path, content) {
    try {
      fs.writeFileSync(path, content)
    } catch (err) {
      console.error(err)
    }
  },
  /**
  检查创建文件夹,支持多个参数
  */
  mkdirSync(...list) {
    list.forEach(dir => {
      if (!fs.existsSync(dir)) fs.mkdirSync(dir)
    })
  }
}
