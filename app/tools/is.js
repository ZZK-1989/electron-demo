const isdev = process.env.NODE_ENV !== "production"
export default {
  dev: isdev, //开发模式
  debug: isdev || process.env.IS_TEST,
  windows: process.platform === "win32"
  // get debug (){
  //   return false
  // }
}
