import { app, shell } from "electron"
import APP from "../tools/appHelper"
import store from "@/store"
import netHelper from "./netHelper"
//下载文件
var _newver = ""
//更新
function install() {
  shell.openExternal(APP.state.resourcesPath + "update.vbs", {
    activate: false,
    workingDirectory: APP.state.resourcesPath
  })
  store.commit("config/setdata", { Version: _newver })
  app.exit()
}
function checkupdate() {
  return new Promise((resolve, reject) => {
    netHelper.get(
      `${APP.state.VUE_APP_publish_url}app.txt?d=${Math.random()}`,
      newver => {
        _newver = newver
        var bigver = app.getVersion()
        var ver = store.state.config.Version //本地版本
        if (ver != newver && newver.indexOf(bigver) == 0) {
          //大版本号相同
          resolve(`${process.env.VUE_APP_publish_url}app.zip` + d)
        }
        return reject("no uodate")
      },
      reject
    )
  })
}
export default () => {
  return new Promise((resolve, reject) => {
    checkupdate().then(url => {
      //有更新
      resolve({
        download: funstate =>
          netHelper.downloadfile(
            url,
            `${APP.state.VUE_APP_publish_url}app.zip?d=${Math.random()}`,
            funstate
          ),
        install
      })
    })
  })
}
