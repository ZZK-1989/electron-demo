import fileHelper from "./fileHelper"
import { app } from "electron"
fileHelper.mkdirSync(`${app.getPath("userData")}/logs`) //创建文件夹
export default `((name)=> {
  const logpath =
    require("electron").remote.app.getPath("userData") +
    "/logs/" +
    name +
    ".log"
  const fs = require("fs")
  fs.existsSync(logpath) && fs.unlinkSync(logpath)
  function _log(type) {
    return function() {
      var str = JSON.stringify([...arguments])
      fs.appendFile(
        logpath,
        new Date().toLocaleString()+' '+type+': '+str+'\\n\\n',
        "utf8",
        () => {}
      )
    }
  }
  ;["log", "dir", "group", "groupEnd", "warn", "error"].forEach(key => {
    console[key] = _log(key)
  })
})`
