var fs = require("fs")
const http = require("http")
const https = require("https")
/**
下载文件
downloadfile(网址,保存地址,进度回调).then().catch(errstr=>{})
*/
function downloadfile(url, path, callback) {
  return new Promise((resolve, reject) => {
    var request = (url.indexOf("https") === 0 ? https : http).get(url, function(
      response
    ) {
      var len = parseInt(response.headers["content-length"], 10)
      var cur = 0
      var tZipWStream = fs.createWriteStream(path)
      response.pipe(tZipWStream)
      response.on("data", function(chunk) {
        cur += chunk.length
        callback(((100 * cur) / len).toFixed(2) - 0)
      })
      response.on("end", function() {
        resolve("ok")
      })
      request.on("error", function(e) {
        reject(e.message)
      })
    })
  })
}
function get(url, success, error) {
  ;(url.indexOf("https") === 0 ? https : http).get(url, function(res) {
    var result = ""
    res.setEncoding("UTF-8")
    res.on("data", function(data) {
      result += data
    })
    res.on("error", error)
    res.on("end", function() {
      success(result)
    })
  })
}
export default {
  get,
  downloadfile
}
