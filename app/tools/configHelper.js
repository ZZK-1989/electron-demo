import file from "./fileHelper";
//配置文件
export default {
  //读取
  read(path) {
    try {
      return JSON.parse(file.readAllTextSync(path)) || {};
    } catch (error) {}
    return {};
  },
  //保存
  save(path, content) {
    file.writeAllTextSync(path, JSON.stringify(content || {}));
  }
};
