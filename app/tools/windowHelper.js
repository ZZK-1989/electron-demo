/**
 * 窗口帮助类
 */
import { BrowserWindow, protocol } from "electron"
import is from "./is"
import electronLog from "./electronLog"
import store from "@/store"
// 开发时也不显示控制台
const hidedevtool = [
  // "tray"
]
//所有页面
const pages = {}
/** 创建窗口
 * url
 * opt:BrowserWindow的参数
 * Setup:{show:true}
 * 可以考虑实现一个窗口池,关闭时不销毁
 */
function createWindow(url, opt = {}, Setup) {
  var setup = Object.assign(
    {
      show: true //自动显示
    },
    Setup
  )
  var win = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true
    },
    ...opt
  })
  setup.show &&
    win.once("ready-to-show", () => {
      // console.log("ready-to-show", new Date().getTime())
      win.show()
    }) //创建完就显示
  win.loadURL(url)

  return win
}
//支持name 或者远程地址
function _geturl(url) {
  if (url.indexOf("http") == 0) return url
  let baseurl = process.env.WEBPACK_DEV_SERVER_URL || "app://."
  return `${baseurl}/${url}.html`
}
export default {
  //通过名称获取页面,有可能为null
  get(name) {
    return pages[name] || null
  },
  /** 打开窗口,不存在的时候创建
   * name 窗口名称
   * url  支持name比如index 或者远程地址https://...
   * opt:BrowserWindow的参数
   * Setup:{show:true}
   */
  open(name, url, opt, setup) {
    var win = this.get(name)
    if (!win) {
      win = createWindow(_geturl(url), opt, setup)
      pages[name] = win
      if (store.state.config.log) {
        //开启日志
        win.webContents.executeJavaScript(`${electronLog}("${name}");`)
      }
      win.once("closed", () => {
        pages[name] = null
      })
    }
    is.debug &&
      hidedevtool.indexOf(name) == -1 &&
      win.webContents.openDevTools()
    return win
  },
  //等待一个页面完成加载
  ready(name) {}
}
