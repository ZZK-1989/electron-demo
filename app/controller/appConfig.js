//程序信息初始化
let { app } = require("electron")
import store from "@/store"
import appHelper from "../tools/appHelper"
import config from "../tools/configHelper"
export default {
  //程序启动前
  beforeCreate() {
    //读取配置文件
    let cf = config.read(`${appHelper.state.userData}config`)
    let ver_curr = app.getVersion() //当前版本
    if (!cf.Version || cf.Version.indexOf(ver_curr) != 0) {
      cf.Version = `${ver_curr}.0`
    }
    store.commit("config/setdata", cf)
  },
  methods: {
    //保存vuex配置到本地文件
    "save-app-config"() {
      config.save(`${appHelper.state.userData}config`, store.state.config)
    }
  }
}
