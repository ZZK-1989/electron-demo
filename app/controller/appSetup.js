/**
 *可选设置
 */
import { app, Menu } from "electron"
export default {
  beforeCreate() {
    process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = "true" //关闭警告信息
    app.commandLine.appendSwitch("--ignore-certificate-errors", "") //关闭证书错误
    if (process.platform === "win32") {
      app.setAppUserModelId(process.env.VUE_APP_appId) //程序的appid 默认使用反域名
    }
    process.on("uncaughtException", function(err) {
      //崩溃情况
      console.log("Caught exception: ", err)
    })
  },
  beforeMount() {
    Menu.setApplicationMenu(null) //隐藏菜单栏.
  }
}
