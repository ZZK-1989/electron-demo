import { app, shell } from "electron"

import windowHelper from "../tools/windowHelper"
let { resolve } = require("path")
import APP from "../tools/appHelper"
import ipc from "electron-ipc-extra"
// 自定义协议
function protocalHandler(handleUrl) {
  const args = []
  if (!app.isPackaged) {
    // 如果是开发阶段，需要把我们的脚本的绝对路径加入参数中
    args.push(resolve(process.argv[1]))
  }
  // 加一个 `--` 以确保后面的参数不被 Electron 处理
  args.push("--")
  // 注册协议
  const PROTOCOL = app.getName()
  app.setAsDefaultProtocolClient(PROTOCOL, process.execPath, args)
  // 如果打开协议时，没有其他实例，则当前实例当做主实例，处理参数
  handleArgv(process.argv)

  // 其他实例启动时，主实例会通过 second-instance 事件接收其他实例的启动参数 `argv`
  app.on("second-instance", (event, argv) => {
    // Windows 下通过协议URL启动时，URL会作为参数，所以需要在这个事件里处理
    if (process.platform === "win32") {
      handleArgv(argv)
    }
  })
  // macOS 下通过协议URL启动时，主实例会通过 open-url 事件接收这个 URL
  app.on("open-url", (event, urlStr) => {
    handleUrl(urlStr)
  })

  // 处理参数
  function handleArgv(argv) {
    const prefix = `${PROTOCOL}:`
    // 开发阶段，跳过前两个参数（`electron.exe .`）
    // 打包后，跳过第一个参数（`myapp.exe`）
    const offset = app.isPackaged ? 1 : 2
    const url = argv.find((arg, i) => i >= offset && arg.startsWith(prefix))
    if (url) handleUrl(url)
  }
}
let Params = { get() {} }
let __handleUrl = urlStr => {
  const urlObj = new URL(urlStr)
  const { searchParams } = urlObj
  Params = searchParams
  var win = windowHelper.get("index")
  if (win != null) {
    //程序已经已经启动时收到参数
    vm.mounted()
  }
}
protocalHandler(__handleUrl)
var vm = {
  //窗口加载完成后
  mounted() {
    var win = windowHelper.get("index")
    if (Params.get("dev")) {
      win.webContents.openDevTools() //如果包含dev 创建窗口后打开调试工具
    }
    // var router = Params.get("url");
    //if (router) {
    // ipc.send("跳转页面",router)//比如有一个参数url,打开窗口就跳转到那里去
    //}
    if (Params.get(app.getName()) == "1") {
      APP.emit("showdemo") //自定义事件
    } else {
      win.show()
    }
  },
  methods: {
    //接收事件
    protocol(url) {
      __handleUrl(url)
    }
  }
}

export default vm
