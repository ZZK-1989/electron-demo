import ipc from "electron-ipc-extra"
import store from "@/store"
// 代码模版;复制使用
export default {
  //程序启动前
  beforeCreate() {},
  //程序启动
  created() {},
  //创建窗口后
  beforeMount() {},
  //窗口加载完成后
  mounted() {},
  //退出程序前
  beforeDestroy() {},
  //退出程序
  destroyed() {},
  methods: {
    //ipc:on 接收事件 async
    // fun(a, b) {
    //   return a + b
    // }
  }
}
