//主进程业务代码
export default [
  require("./appConfig").default, //程序配置文件
  require("./appSetup").default, //一般设置
  require("./protocol").default, //自定义协议,通过链接调用程序
  require("./appUpdate").default, //自动更新&热更新
  require("./appTray").default, //托盘图标
  require("./demo").default //demo内容
]
