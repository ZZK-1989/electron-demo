/**
 * 自动更新
 *
 */
let { app } = require("electron")
// 自动更新相关
let { autoUpdater } = require("electron-updater")
autoUpdater.autoDownload = true //自动下载更新
import ipc from "electron-ipc-extra"
import windowHelper from "../tools/windowHelper"

//设置下载进度(任务栏和程序界面)
function setprogressbar(i) {
  ipc.send("setProgressBar", i.toFixed(2) - 0) //设置下载进度
  i /= 100
  if (i >= 1) i = 1
  var win = windowHelper.get("index")
  win.setProgressBar(i.toFixed(2) - 0) //任务栏进度
}
// 当更新出现错误时触发
autoUpdater.on("error", err => {
  ipc.send("UpdateErr", err)
})
// 当开始检查更新的时候触发
autoUpdater.on("checking-for-update", () => {
  console.log("checking:")
})
// 当发现一个可用更新的时候触发，更新下载包会自动开始
autoUpdater.on("update-available", info => {
  console.log("start download")
  autoUpdater.downloadUpdate()
})
// 当没有可用更新的时候触发 检查资源更新
autoUpdater.on("update-not-available", info => {
  console.log("updateNotAva", "当没有可用更新")
  var webupdater = require("../tools/webupdater").default //小版本更新
  webupdater().then(({ download, install }) => {
    download(setprogressbar)
      .then(install)
      .catch(err => {
        ipc.send("UpdateErr", err)
      })
  })
})
// 更新下载进度事件
autoUpdater.on("download-progress", progressObj => {
  var jd = (progressObj && progressObj.percent) || 0
  console.log("downloadProgress", jd)
  setprogressbar(jd)
})
autoUpdater.on("update-downloaded", info => {
  // 下载太快可能无法触发downloadProgress事件，所以手动通知一下
  setprogressbar(100)
  autoUpdater.quitAndInstall()
  console.log("start installation")
})

export default {
  mounted() {
    if (app.isPackaged) {
      autoUpdater.checkForUpdates()
    }
  },
  methods: {
    checkForUpdate() {
      console.log("check-update")
      autoUpdater.checkForUpdates()
    },
    //下载新版本
    Download_new_version() {}
  }
}
