/**
 * 系统托盘
 */
let { app, Menu, Tray, nativeImage } = require("electron");
import APP from "../tools/appHelper"
let tray = null;
//这里改成使用vuex-i18n 就可以直接通过vuex切换语言
let createTray = (lang = "zh_CN") => {
  let win = APP.state.mainwin;
  let __menu = [
    {
      label: lang == "en" ? "Display main window" : "显示主窗口",
      click: function() {
        win.show();
      }
    },
    {
      label: (lang == "en" ? "current version" : "当前版本") + app.getVersion(),
      click() {}
    },
    {
      label: lang == "en" ? "Exit" : "退出",
      click: function() {
        app.exit();
      }
    }
  ];
  if (tray != null) return tray.setContextMenu(Menu.buildFromTemplate(__menu)); //更新托盘内容
  tray = new Tray(nativeImage.createFromPath(APP.state.appFolder + "icon.ico"));
  tray.setToolTip(app.name);
  tray.setContextMenu(Menu.buildFromTemplate(__menu));
  tray.on("click", () => {
    win.isVisible() ? win.hide() : win.show();
  });
};
let Destroy = () => {
  tray && tray.destroy(), (tray = null);
};
export default {
  //窗口加载完成后
  mounted() {
    createTray()
    app.on("before-quit", () => {
      Destroy();
    });
  },
  //退出程序前
  destroyed() {
    //清理
    Destroy();
  },
  methods: {
    /**
    设置托盘状态
    新消息等
    */
    "tray-set-status"(type) {
      console.log(type)
      // switch (type) {
      //   case value:
      //     break

      //   default:
      //     break
      // }
    },
    /**
    退出程序
    */
    "tray-exit"() {
      app.quit()
    }
  }
};
