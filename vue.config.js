let path = require("path")
module.exports = {
lintOnSave: undefined,
  productionSourceMap: false,
  runtimeCompiler: false,
  // 跨域配置
  devServer: {
    disableHostCheck: true,
    overlay: {
      warnings: false,
      errors: true
    }
  },
  chainWebpack: config => {
    config.plugins.delete("prefetch")
  },
  parallel: require("os").cpus().length > 1,
  configureWebpack: config => {
    Object.assign(config, {
      resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
          "@": path.resolve("src"),
          components: path.resolve("src/components"),
          assets: path.resolve("src/assets")
        }
      },
      externals: {}
    })
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [path.resolve("src/styles/var.less")]
    },
    electronBuilder: {
      // Use this to change the entrypoint of your app's main process
      mainProcessFile: "app/index.js",
      // Provide an array of files that, when changed, will recompile the main process and restart Electron
      // Your main process file will be added by default
      mainProcessWatch: ["app/*/*.js"],
      // removeElectronJunk: false,
      externals: [
        // "ffi",
        // "ref",
        // "ref-struct",
        // "ref-array",
        // "iconv-lite",
        // "node-machine-id",
        // "regedit"
      ],
      nodeModulesPath: ["../../node_modules", "./node_modules"],
      builderOptions: {
        productName: "demo",
        appId: process.env.VUE_APP_appId,
        copyright: "Copyright © 2019 武汉薄荷科技有限公司",
        artifactName: "${productName}_${version}.${ext}",
        buildDependenciesFromSource: false,
        npmRebuild: false,
        directories: {},
        extraFiles: [
          {
            from: "./static/",
            to: "./"
          }
        ],
        win: {
          // windows相关的配置
          icon: "./static/icon.ico", //图标，当前图标在根目录下，注意这里有两个坑
          requestedExecutionLevel: "highestAvailable",
          target: [
            {
              target: "nsis", //利用nsis制作安装程序
              arch: [
                "ia32" //32位
              ]
            }
          ]
          //  rfc3161TimeStampServer: "http://tsa.startssl.com/rfc3161",
          //sign: "./build/sign/index.js"
          //FIXME 【打包打开】不签名需要注释下面几行代码
          // rfc3161TimeStampServer:
          //   "http://timestamp.globalsign.com/?signature=sha2",
          // verifyUpdateCodeSignature: false,
          // certificateSubjectName: "武汉薄荷科技有限公司",
          // signDlls: false,
          // signingHashAlgorithms: ["sha1", "sha256"],
          // certificateSha1: "8260fc497a408a8701022b479aa4fbcd79a92b13"
        },
        mac: {
          // category: "public.app-category.productivity",
          //icon: "./static/icons/256x256.png"
        },
        dmg: {
          background: "./static/appdmg.png",
          icon: "./static/icon.icns",
          iconSize: 100,
          contents: [
            {
              x: 380,
              y: 280,
              type: "link",
              path: "/Applications"
            },
            {
              x: 110,
              y: 280,
              type: "file"
            }
          ]
        },
        linux: {
          category: "Chat;GNOME;GTK;Network;InstantMessaging",
          packageCategory: "GNOME;GTK;Network;InstantMessaging",
          description: "Zulip Desktop Client for Linux",
          target: ["deb", "zip", "AppImage", "snap"]
          // "artifactName": "test."
        },
        nsis: {
          oneClick: false, // 是否一键安装
          allowElevation: true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          perMachine: true,
          allowToChangeInstallationDirectory: true, // 允许修改安装目录
          installerIcon: "./static/icon.ico", // 安装图标
          uninstallerIcon: "./static/icon.ico", //卸载图标
          installerHeaderIcon: "./static/icon.ico", // 安装时头部图标
          createDesktopShortcut: true, // 创建桌面图标
          createStartMenuShortcut: true, // 创建开始菜单图标
          shortcutName: "NN", // 图标名称
          uninstallDisplayName: "雷神NN",
          include: "./install.nsi" //安装脚本
        },
        publish: [
          {
            provider: "generic",
            url: process.env.VUE_APP_publish_url
          }
        ]
      }
    }
  }
}
