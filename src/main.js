import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"

Vue.config.productionTip = false

Vue.use(require("./utils/ipc").default) //进程交互功能

require("./utils/protocolcheck") //判断一个自定义协议是否安装/

new Vue({
  router,
  store,
  render: function(h) {
    return h(App)
  }
}).$mount("#app")
