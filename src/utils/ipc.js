import store from "../store";
let ipc;
export default Vue => {
  if (process.env.IS_ELECTRON) {
    ipc = require("electron-ipc-extra");
    var events = require("./ipcEvent").default(ipc);
    var ipcWatch = require("./ipcWatch").default(ipc),
      __watchkeys = Object.keys(ipcWatch);
    store.subscribe(({ type, payload }, state) => {
      let i = __watchkeys.indexOf(type);
      if (i > -1) {
        ipcWatch[type]({ type, payload }, state);
      }
    });

    Object.keys(events).forEach(key => {
      ipc.on(key, events[key]);
    });
    ipc.on("vuex$commit", ({ type, payload }) => {
      store.commit(type, payload);
    });
    ipc.on("vuex$dispatch", ({ type, payload }) => {
      return store.dispatch(type, payload);
    });
    ipc.send("$$AppReady");
  } else {
    ipc = {
      on() {},
      send: () => Promise.reject(0)
    };
  }
  //vue 中调用主进程this.$ipc(name,参数1,参数2,...参数).then((data)=>{})
  Vue.prototype.$ipc = (...args) => ipc.send.apply(ipc, args);
  //vuex 中调用主进程  dispatch("$ipc",[name,参数1,参数2,...参数]).then((data)=>{})
  store.registerModule("ipc", {
    actions: {
      $ipc: (e, data) => ipc.send.apply(ipc, data)
    }
  });
};
