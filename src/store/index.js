import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
export default new Vuex.Store({
  plugins: [
    ...(!process.env.VUE_APP_ISWEB //非浏览器环境下加载vuex-electron
      ? [require("vuex-electron").createPersistedState()]
      : [])
  ],
  modules: {
    config: require("./config").default, //程序配置-会保存到文件%appData%/config
    demo: require("./demo").default
  }
});
