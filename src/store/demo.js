export default {
  namespaced: true,
  state: {
    //程序版本号
    id: 0
  },
  mutations: {
    push(s) {
      s.id++;
      console.log("调用次数", s.id);
    }
  },
  actions: {
    async push({ commit, dispatch, state }) {
      commit("push");
      var c = await dispatch("$ipc", ["push", state.id], { root: true });
      console.log(c);
    }
  }
};
