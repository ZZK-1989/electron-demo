export default {
  namespaced: true,
  state: {
    //程序版本号
    Version: "",
    log: 0 //日志输出到文件1 appData\Roaming\nn\logs\
  },
  mutations: {
    setdata(s, db) {
      Object.assign(s, db);
    }
  },
  actions: {
    setdata({ commit, dispatch }, db) {
      commit("setdata", { Version: "2" })
      // dispatch("$ipc",["save"]});
    }
  }
};
